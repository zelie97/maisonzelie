const express = require("express");
const app = express();
const userRoute = express.Router();
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
const User = require("../models/User");
const authorize = require("../middlewares/auth");
const { check, validationResult } = require("express-validator");

// Sign up
userRoute.post(
  "/create",
  [
    check("lastname")
      .not()
      .isEmpty()
      .withMessage("Lastname is required"),
    check("firstname")
      .not()
      .isEmpty()
      .withMessage("Firstname is required"),
    check("adress")
      .not()
      .isEmpty()
      .withMessage("Adress is required"),
    check("zipcode")
      .not()
      .isEmpty()
      .withMessage("Zipcode is required"),
    check("city")
      .not()
      .isEmpty(),
    check("email", "Email is required")
      .not()
      .isEmpty(),
    check("password", "Password should be between 5 to 8 characters long")
      .not()
      .isEmpty()
      .isLength({ min: 5, max: 8 }),
    check("phonenumber")
      .not()
  ],
  (req, res, next) => {
    const userData = req.body;
    const errors = validationResult(req);
    console.log(userData);

    if (!errors.isEmpty()) {
      return res.status(422).jsonp(errors.array());
    } else {
      bcrypt.hash(userData.password, 10).then(hash => {
        const user = new User({
          lastname: userData.lastname,
          firstname: userData.firstname,
          adress: userData.adress,
          zipcode: userData.zipcode,
          city: userData.city,
          phonenumber: userData.phonenumber,
          email: userData.email,
          password: hash
        });
        user
          .save()
          .then(response => {
            res.status(201).json({
              message: "User successfully created!",
              result: response
            });
          })
          .catch(error => {
            res.status(500).json({
              error: error
            });
          });
      });
    }
  }
);

// Sign-in
userRoute.post("/login", (req, res, next) => {
  let getUser;
  User.findOne({
    email: req.body.email
  })
    .then(user => {
      if (!user) {
        return res.status(401).json({
          message: "Authentication failed"
        });
      }
      getUser = user;
      return bcrypt.compare(req.body.password, user.password);
    })
    .then(response => {
      if (!response) {
        return res.status(401).json({
          message: "Authentication failed"
        });
      }
      let jwtToken = jwt.sign(
        {
          email: getUser.email,
          userId: getUser._id
        },
        "longer-secret-is-better",
        {
          expiresIn: "2h"
        }
      );
      res.status(200).json({
        token: jwtToken,
        expiresIn: 7200,
        msg: getUser
      });
    })
    .catch(err => {
      return res.status(401).json({
        message: "Authentication failed"
      });
    });
});

// Get All Users
userRoute.get(
  "/",
  (authorize,
  (req, res) => {
    User.find((error, data) => {
      if (error) {
        return next(error);
      } else {
        res.status(200).json(data);
      }
    });
  })
);

// Get single user
userRoute.get(
  "/read/:id",
  (authorize,
  (req, res, next) => {
    User.findById(req.params.id, (error, data) => {
      if (error) {
        return next(error);
      } else {
        res.status(200).json({
          msg: data
        });
      }
    });
  })
);

// Update user
userRoute.put(
  "/update/:id",
  (authorize,
  (req, res, next) => {
    User.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: req.body },
      { new: true },
      (error, data) => {
        if (error) {
          return next(error);
        } else {
          res.json(data);
        }
      }
    );
  })
);

// Delete user
userRoute.delete(
  "/delete/:id",
  (authorize,
  (req, res, next) => {
    User.findOneAndRemove({ _id: req.params.id }, (error, data) => {
      if (error) {
        return next(error);
      } else {
        res.status(200).json({
          msg: data
        });
      }
    });
  })
);

module.exports = userRoute;
