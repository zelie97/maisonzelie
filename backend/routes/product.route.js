const express = require("express");
const app = express();
const productRoute = express.Router();

// Product model
let Product = require("../models/Product");

// Add Product
productRoute.post("/create", (req, res, next) => {
  let productData = req.body;
  Product.create(productData, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data);
    }
  });
});

// Get All Products
productRoute.get("/", (req, res) => {
  Product.find((error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data);
    }
  });
});

// Get single product
productRoute.get("/read/:id", async (req, res) => {
  await Product.findById(req.params.id, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.json(data);
    }
  });
});

// Update product
productRoute.put("/update/:id", (req, res, next) => {
  Product.findByIdAndUpdate(
    { _id: req.params.id },
    { $set: req.body },
    { new: true },
    (error, data) => {
      if (error) {
        return next(error);
      } else {
        res.json(data);
      }
    }
  );
});

// Delete product
productRoute.delete("/delete/:id", (req, res, next) => {
  Product.findOneAndDelete({ _id: req.params.id }, (error, data) => {
    if (error) {
      return next(error);
    } else {
      res.status(200).json({
        msg: data
      });
    }
  });
});

module.exports = productRoute;
