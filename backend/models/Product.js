const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Define collection and schema
let Product = new Schema(
  {
    productId: {
      type: Number
    },
    title: {
      type: String
    },
    description: {
      type: String
    },
    price: {
      type: Number
    },
    image: {
      type: String
    },
    category: {
      type: String
    }
  },
  
  {
    collection: "products"
  }
);

module.exports = mongoose.model("Product", Product);
