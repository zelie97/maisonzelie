const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const uniqueValidator = require("mongoose-unique-validator");
// Define collection and schema
let User = new Schema(
  {
    lastname: {
      type: String
    },
    firstname: {
      type: String
    },
    adress: {
      type: String
    },
    zipcode: {
      type: String
    },
    city: {
      type: String
    },
    phonenumber: {
      type: String
    },
    email: {
      type: String,
      unique: true
    },
    password: {
      type: String
    }
  },
  {
    collection: "users"
  }
);
User.plugin(uniqueValidator, { message: "Email already in use." });
module.exports = mongoose.model("User", User);
