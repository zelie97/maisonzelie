import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { AdminComponent } from "./admin/admin.component";
import { AdminProductsComponent } from "./admin/admin-products/admin-products.component";
import { EditProductsComponent } from './admin/admin-products/edit-products/edit-products.component';
import { AdminUsersComponent } from "./admin/admin-users/admin-users.component";
import { ProductsComponent } from "./products/products.component";
import { CartComponent } from "./products/cart/cart.component";
import { UserComponent } from "./user/user.component";
import { SignInComponent } from "./user/sign-in/sign-in.component";
import { SignUpComponent } from "./user/sign-up/sign-up.component";
import { HomeComponent } from "./page/home/home.component"

const routes: Routes = [
  { path: "", component: HomeComponent },

  /**
   * User routes
   */
  { path: "user", component: UserComponent },
  { path: "user/signin", component: SignInComponent },
  { path: "user/signup", component: SignUpComponent },

  /**
   * Products routes
   */
  { path: "products", component: ProductsComponent },
  { path: "products/cart", component: CartComponent },
  

  /**
   * Admin routes 
   */
  { path: "admin", component: AdminComponent },
  { path: "admin/products", component: AdminProductsComponent },
  { path: "admin/products/edit/:id", component: EditProductsComponent },
  { path: "admin/users", component: AdminUsersComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
