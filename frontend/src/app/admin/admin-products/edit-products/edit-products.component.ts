import { Component, OnInit, Input } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { ProductService } from "../../../products/Services/product.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-edit-products",
  templateUrl: "./edit-products.component.html",
  styleUrls: ["./edit-products.component.scss"]
})
export class EditProductsComponent implements OnInit {
  productId: any;
  singleProduct: {
    title: "";
    description: "";
    price: 1;
    image: "";
    category: "";
  };
  constructor(
    private productService: ProductService,
    private route: ActivatedRoute
  ) {
    this.route.params.subscribe(p => {
      this.productId = p.id;
    });
    this.productService
      .getProductById(this.productId)
      .subscribe(async (res: any) => {
        console.log(res);
        this.singleProduct = await res;
        this.editProduct = new FormGroup({
          title: new FormControl(this.singleProduct.title),
          description: new FormControl(this.singleProduct.description),
          price: new FormControl(this.singleProduct.price),
          image: new FormControl(this.singleProduct.image),
          category: new FormControl("")
        });
      });
  }

  ngOnInit() {}
  editProduct = new FormGroup({
    title: new FormControl(""),
    description: new FormControl(""),
    price: new FormControl(""),
    image: new FormControl(""),
    category: new FormControl("")
  });

  onSubmit() {
    console.log(this.editProduct.value);
    this.productService
      .editProducts(this.editProduct.value, this.productId)
      .subscribe((res: any) => {
        console.log(res);
      });
  }
}
