import { Component, OnInit } from "@angular/core";
import { FormGroup, FormControl } from "@angular/forms";
import { ProductService } from "../../products/Services/product.service";

@Component({
  selector: "app-admin-products",
  templateUrl: "./admin-products.component.html",
  styleUrls: ["./admin-products.component.scss"]
})

export class AdminProductsComponent implements OnInit {
  
  private products: any[];
  constructor(private productService: ProductService) {}

  ngOnInit() {
    this.productService.getProducts().subscribe((data: any) => {
      this.products = data;
    });
  }

  newProduct = new FormGroup({
    title: new FormControl(""),
    description: new FormControl(""),
    price: new FormControl(""),
    image: new FormControl(""),
    category: new FormControl("")
  });

  onSubmit() {
    console.log(this.newProduct.value);
    this.productService
      .addProducts(this.newProduct.value)
      .subscribe((res: any) => {
        console.log(res);
      });
  }

  deleteProducts(productId) {
    if (window.confirm("Etes vous sûr de vouloir supprimer?")) {
      this.productService.deleteProducts(productId).subscribe(() => {
        this.productService.getProducts().subscribe((data: any) => {
          this.products = data;
        });
      });
    }
  }
}

