import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"]
})
export class UserComponent implements OnInit {
  
  _id: String;
  lastName: String;
  name: String;
  adress: String;
  zipcode: Number;
  city: String;
  pseudo: String;
  email: String;
  password: String;
  phoneNumber: Number;

  constructor() {}

  ngOnInit() {}
}
