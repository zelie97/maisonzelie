import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from "@angular/forms";
import { UserService } from "../services/user.service";

@Component({
  selector: "app-sign-up",
  templateUrl: "./sign-up.component.html",
  styleUrls: ["./sign-up.component.scss"]
})
export class SignUpComponent implements OnInit {
  private user: any[];
  constructor(private userService: UserService) {}

  ngOnInit() {}

  newUser = new FormGroup({
    lastname: new FormControl(""),
    firstname: new FormControl(""),
    adress: new FormControl(""),
    zipcode: new FormControl(""),
    city: new FormControl(""),
    phonenumber: new FormControl(""),
    email: new FormControl(""),
    password: new FormControl("")
  });

  onSubmit() {
    console.log(this.newUser.value);
    this.userService
      .addUser(this.newUser.value)
      .subscribe((res: any) => {
        console.log(res);
      });
}
}