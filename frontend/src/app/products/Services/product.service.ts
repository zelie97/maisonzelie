import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class ProductService {
  baseUrl: String = "http://localhost:4000";

  constructor(private http: HttpClient) {}

  //Create product
  addProducts(product: any) {
    return this.http.post(this.baseUrl + "/product/create", product);
  }

  //Read all products
  getProducts() {
    return this.http.get(this.baseUrl + "/product");
  }

  //Read one product
  getProductById(id: any) {
    return this.http.get(this.baseUrl + "/product/read/" + id);
  }

  //Update product
  editProducts(product: any, id: any) {
    return this.http.put(this.baseUrl + "/product/update/" + id, product);
  }

  //Delete product
  deleteProducts(id: any) {
    return this.http.delete(this.baseUrl + "/product/delete/" + id);
  }

  // // Error handling 
  // errorMgmt(error: HttpErrorResponse) {
  //   let errorMessage = '';
  //   if (error.error instanceof ErrorEvent) {
  //     // Get client-side error
  //     errorMessage = error.error.message;
  //   } else {
  //     // Get server-side error
  //     errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
  //   }
  //   console.log(errorMessage);
  //   return throwError(errorMessage);
  // }

}

