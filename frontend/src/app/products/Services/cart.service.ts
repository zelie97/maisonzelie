import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  baseUrl: String = "http://localhost:4000";
  constructor(private http: HttpClient) { }

  getProduct(productId: string){
    return this.http.get(this.baseUrl + "/read/" + productId)
  }
}
